// 1) var имеет глобальную область видимости. на даннный момент считается устаревшим

// let более современный способ объявления переменной, имеет область видимости в блоке, в котором

//она объявлена.

// и var и let могуть быть перезаписаны по ходу ходу

// переменная const работает также как и let, но с учетом что данный в ней являются константой и не могут быть
// перезаписаны(но если объект объявлен через const то он может быть без проблем изменен.

// 2) var считается устаревшим способом, великие джаваскрипт ученые изобрели и доказали превосходство let
// var выполняется первоочередно и его область видимости по всему коду, что
// не всегда требуется


let userFirstName = prompt('Enter your name: ');
let pattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/);
let numericPattern = new RegExp (/[0-9]/);
while (!userFirstName || pattern.test(userFirstName) || numericPattern.test(userFirstName)) {
    userFirstName = prompt(`Enter Your Name again!`, `${userFirstName}`);
}

let userAge = +prompt('Enter your age: ');
while (Number.isNaN(userAge) || userAge === false || userAge < 0 || userAge > 200) {
    userAge = +prompt('Enter your age correct as number: ', `${userAge}`);
}


if (userAge <= 18) {
    alert(`You are not allowed to visit this website`);
}

if (userAge <= 22 && userAge >= 18) {
    let userDecision = confirm(`Are you sure you want to continue?`);
    if (userDecision === true) {
        alert(`Welcome ${userFirstName}!`);
    } else {
        alert(`You are not allowed to visit this website`);
    }
}

if (userAge > 22) {
    alert(`Welcome ${userFirstName}!`);
}



